<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Docker LAMP Composer Structure - Test Database connection</title>
</head>
<body>
    <h1>Docker LAMP Composer Structure - Test Database connection</h1>
    <ul>
        <li><a href="/index.html">Accueil</a></li>
        <li><a href="/tests/phpinfo.php">PHP Info</a></li>
        <li><a href="/tests/test-db.php">Test Database</a></li>
        <li><a href="/tests/test-mailer.php">Test Mailer</a></li>
    </ul>
    <hr />
    <p>Attempting MySQL connection from php...</p>
<?php 
    $host = 'db';
    $user = 'user';
    $pass = 'pwd';
    $conn = new mysqli($host, $user, $pass);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    echo "Connected to MySQL successfully!";
?>
</body>
</html>