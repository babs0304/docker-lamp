<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Docker LAMP Composer Structure - Test Mailer</title>
</head>
<body>
    <h1>Docker LAMP Composer Structure - Test Mailer</h1>
    <ul>
        <li><a href="/index.html">Accueil</a></li>
        <li><a href="/tests/phpinfo.php">PHP Info</a></li>
        <li><a href="/tests/test-db.php">Test Database</a></li>
        <li><a href="/tests/test-mailer.php">Test Mailer</a></li>
    </ul>
<?php

    // Start with PHPMailer class
    use PHPMailer\PHPMailer\PHPMailer;
    require_once './vendor/autoload.php';

    echo("Mailer check...");

    // create a new object
    $mail = new PHPMailer();
    // configure an SMTP
    $mail->isSMTP();
    $mail->Host = 'mail';
    $mail->SMTPAuth = false;
    // $mail->Username = 'api';
    // $mail->Password = '1a2b3c4d5e6f7g';
    // $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mail->Port = 1025;

    $mail->setFrom('god@univers.world', 'Your Hotel');
    $mail->addAddress('e@ma.il', 'Me');
    $mail->Subject = 'Test Mailer';
    // Set HTML 
    $mail->isHTML(TRUE);
    $mail->Body = '<html>Hi there, we are happy to <br>confirm your booking.</br> Please check the document in the attachment.</html>';
    $mail->AltBody = 'Hi there, we are happy to confirm your booking. Please check the document in the attachment.';
    // add attachment 
    // just add the '/path/to/file.pdf'
    $attachmentPath = './confirmations/yourbooking.pdf';
    if (file_exists($attachmentPath)) {
        $mail->addAttachment($attachmentPath, 'yourbooking.pdf');
    }

    // send the message
    if(!$mail->send()){
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent';
    }
?>

</body>
</html>