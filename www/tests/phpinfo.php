<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Docker LAMP Composer Structure - PHP Info</title>
</head>
<body>
    <h1>Docker LAMP Composer Structure - PHP Info</h1>
    <ul>
        <li><a href="/index.html">Accueil</a></li>
        <li><a href="/tests/phpinfo.php">PHP Info</a></li>
        <li><a href="/tests/test-db.php">Test Database</a></li>
        <li><a href="/tests/test-mailer.php">Test Mailer</a></li>
    </ul>
    <?php
        phpinfo();
    ?>    
</body>
</html>
